# Pure-FTP Docker Container
----
## Start container
First open docker-compose.yml and change "PUBLICHOST" to your NAT ip address (usually 192. or 10. network), then run:
> docker-compose up

## Create User
kev:password
> $ docker-compose exec pureftpd /scripts/add_user.sh

## Test ftp server
> $ ftp -p <your-ip>
Name: kev
Password: <password you entered when created>
ftp> !touch file.txt
ftp> !ls
ftp> put file.txt
ftp> !rm file.txt
ftp> get file.txt
ftp> del file.txt
ftp> ls
ftp> bye

# Starting over
> $ docker-compose down
$ rm -f data/pure-ftpd/pureftpd.passwd
$ rm -rf data/ftpuser/kev

Now you have a blank ftp container to start with
