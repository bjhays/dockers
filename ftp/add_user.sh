#!/bin/bash
# $ docker-compose exec pureftpd /scripts/add_user.sh
echo "add user 'kev'"
pure-pw useradd kev -u ftpuser -d /home/ftpuser/kev -t 1024 -T 1024 -y 1 -m
pure-pw list
pure-pw show kev
# pure-pw passwd kev -m
# pure-pw userdel kev -m
pure-ftpwho -n
exit
