# Samba Docker Container
----
## Start container
> docker-compose up

## Test smb server
> $ smbutil view -A smb://smbuser@<your-ip>
Password for <your-ip>: smbpassword
Authenticate successfully with smb://smbuser@<your-ip>

Mount shares to look around
 - mkdir ~/test_smb/

### smbuser access to shared
> $ mount -t smbfs //smbuser:smbpassword@<your-ip>/shared ~/test_smb
$ ls -l ~/test_smb/
$ umount ~/test_smb/

### smbuser access to private
> $ mount -t smbfs //smbuser:smbpassword@<your-ip>/smbuser_private ~/test_smb
This is a different dir
$ ls -l ~/test_smb/

### guest access to shared
> $ mount -t smbfs //guest@<your-ip>/shared_guest ~/test_smb
$ ls -l ~/test_smb/

# Starting over
> $ docker-compose down
