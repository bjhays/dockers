# CMD echo "add jsmith"; use admin; db.auth(siteUserAdmin, "PASSWORD"); mongo admin --eval "use admin; db.createUser({ user: 'jsmith', pwd: 'some-initial-password', roles: [ { role: 'AdminAnyDatabase', db: 'admin' } ] });"
URL="mongodb:28017"
echo "Wait for $URL"
while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' $URL)" != "200" ]]; do echo "." && sleep 5; done

mongoimport --host mongodb --db admin --collection mydatabase --type json --file /init.json --jsonArray
