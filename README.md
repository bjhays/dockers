# Dockers
A collection of docker/docker-compose so that I don't have to rebuild them every time :-D

## using the api on OSX use socat
socat -d TCP-LISTEN:2376,reuseaddr,fork UNIX:/var/run/docker.sock
